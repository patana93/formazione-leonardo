
import java.lang.String;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonCreator;
/**
 *  Rappresenta un'aula, luogo fisico dove si svolge un corso 
 *	
*/

public class Aula {
    private String id;
    private List<GregorianCalendar> disponibilita;
    private final int posti = 20;
    /**
     * Inizializza un istanza di Aula
     * 
     * @param id 						String
     * @param disponibilitaAulaList 	List
     * 
     * @see java.util.GregorianCalendar
     */
    @JsonCreator
    public void initAula(String id, List<GregorianCalendar> disponibilitaAulaList) {
        this.disponibilita = disponibilitaAulaList;
        this.id = id;
    }
    /**
     * Consente di accedere all'attributo id
     * 
     * @return 	String id
     * @see 	java.lang.String
     */
    public String getId() {
        return this.id;
    }
    /**
     * Consente di accedere alla disponibilit� dell'aula
     * 
     * @return	diponibilita List
     * @see 	java.util.GregorianCalendar
     */
    public List<GregorianCalendar> getDisponibilita(){
        return this.disponibilita;
    }
    /**
     * Metodo che mostra a video il calendario mensile; 
     * in corrispondenza del giorno si mostra "-" se l'aula � occupata
     * 
     * @param month int
     * @return out String
     * @see		java.util.Calendar
     */
    public String printCalendar(int month) {
    	String out = "";
    	
    	
    	// inizializza la data corrente e prendi solo l'anno
        int year = Calendar.getInstance().get(Calendar.YEAR);        
        GregorianCalendar calendar = new GregorianCalendar();
        //GregorianCalendar indica i mesi con numeri da 0 a 11, allineiamo la numerazione dei mesi con l'input da tastiera
        calendar.set(year, month - 1, 0);        
        //set del primo giorno del mese
        calendar.set(GregorianCalendar.DAY_OF_MONTH, 1);      
        //Vedi che giorno della settimana � il primo di questo mese
        int dayOfWeek = calendar.get(GregorianCalendar.DAY_OF_WEEK);       
        // prendi l'ultimo giorno del mese       
        int daysInMonth = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

      
        //Stampa a video il nome del mese, l'anno i giorni della settimana
        System.out.println(new SimpleDateFormat("MMMM YYYY").format(calendar.getTime()));
        System.out.println("  S  M  T  W  T  F  S  ");        
         
        //Inserisci tanti spazi iniziali fino ad arrivare al primo giorno del mese
        String spazioIniziale = "";
        for (int i = 0; i < dayOfWeek - 1; i++) {
            spazioIniziale += "   ";
        }
        out = (spazioIniziale);

        
        GregorianCalendar giorno = new GregorianCalendar();
        
       
        //stampa i giorni del mese a partire dal 1 del mese
        for (int i = 0, dayOfMonth = 1; dayOfMonth <= daysInMonth; i++) {
            for (int j = ((i == 0) ? dayOfWeek - 1 : 0); j < 7 && (dayOfMonth <= daysInMonth); j++) {
                
                giorno.clear();
                giorno.set(year, month - 1, dayOfMonth);
                // se il giorno considerato � contenuto nella lista disponibilit� l'aula � occupata 
                boolean aulaNonDisponibile = this.disponibilita.contains(giorno);
                String giornoString = Integer.toString(dayOfMonth);
                
                if(aulaNonDisponibile) {
                    giornoString = "-";
                }
                out += String.format("%3s", giornoString);
                //System.out.printf("%2s ", giornoString);
                dayOfMonth++;
            }
            out += "\n";
        }
        return out;
    }
    /**
     * permette di inserire una nuova data non disponibile
     * 
     * @param data  GregorianCalendar
     */
    public void addCalendar(GregorianCalendar data) {
        this.disponibilita.add(data);
    }
    /**
     * permette di accedere all'attributo posti
     * 
     * @return int posti
     */
    public int posti() {
        return this.posti;
    }
}
