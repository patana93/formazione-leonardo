/**
 * 
 *<p> Interfaccia che specifica metodi comuni
 * a tutte le boundaries.
 * </p>
 * Specifica i seguenti metodi:
 * <PRE>
 * public void printErr();
 * </PRE>
 */
public interface GuiInterface {
	public void printErr();
}
