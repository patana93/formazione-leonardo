import java.lang.String;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Calendar;




enum Stato {
    NON_SCADUTO,
    IN_SCADENZA, 
    SCADUTO;
}

/**
 *Classe che rappresenta un dipendente. 
 *E' una specializzazione della superclasse Persona
 */
public class Dipendente extends Persona {
    private int matricola;
    private String mansione;
    private String certificato;
    private Calendar scadenza;
     @JsonIgnore
    private Stato stato;
    
     /**
      * Metodo per inizializzare un dipendente
      * 
      * @param matricola    int 
      * @param nome         String
      * @param cognome      String
      * @param mansione     String 
      * @param certificato  String
      * @param year         int 
      * @param month        int 
      * @param day          int 
      * @param mail         String 
      */
     @JsonCreator
    public void initDip(int matricola, String nome, String cognome, String mansione, String certificato, int year,
            int month, int day, String mail) {
    	super.PersonaInit(nome, cognome, mail);
        this.matricola = matricola;
        this.mansione = mansione;
        this.certificato = certificato;
        this.scadenza = Calendar.getInstance();
        this.stato = Stato.IN_SCADENZA;
        // Il mese viene decrementato di 1 perch� il tipo Calendar
        // conta i mesi a partire da 0. Ex: Jan = 0;
        this.scadenza.set(year, month - 1, day);
        
    }
    /**
     * Metodo per aggiornare lo stato di un dipendente
     * 
     * @param scad  Calendar 
     */

    public void setStato(Calendar scad) {
        Calendar adesso = Calendar.getInstance();
        Calendar traTreMesi = Calendar.getInstance();
        traTreMesi.add(Calendar.MONTH, 3);

        if (scad.before(adesso)) {
            this.stato = Stato.SCADUTO;
        } else if (scad.before(traTreMesi)) {
            this.stato = Stato.IN_SCADENZA;
        } else {
            this.stato = Stato.NON_SCADUTO;
        }
    }
/**
 * Metodo che accede alla matricola, identificativo univoco del dipendente.
 * 
 * @return int matricola
 */
    public int getMatricola() {
        return this.matricola;
    }
    
    /**
     * Metodo che accede al nome
     * 
     * @return String nome
     */
    public String getNome() {
        return this.nome;
    }
    /**
     * Metodo che accede al cognome
     * 
     * @return String cognome
     */
    public String getCognome() {
        return this.cognome;
    }
    
/**
 * Metodo che accede alla mansione svolta attualmente dal dipendente
 * 
 * @return String mansione
 */
    public String getMansione() {
        return this.mansione;
    }
    /**
     * Metodo che accede al certificato conseguito a valle di un corso.
     * 
     * @return String certificato
     */
    public String getCertificato() {
        return this.certificato;
    }
    /**
     * Metodo che accede all'enum Stato
     * 
     *  possibili valori di ritorno:
     * SCADUTO			
     * IN_SCADENZA
     * NON_SCADUTO
     * 
     * @return Stato stato
     */
    public Stato getStato() {
        return this.stato;
    }
    /**
     * Metodo che accede alla scadenza del certificato
     * 
     * @return Calendar scadenza
     */
    public Calendar getScadenza() {
        return this.scadenza;
    }
   
	/**
     * Metodo che accede al recapito(mail).
     * 
     * @return String mail
     */
	public String getMail() {
		return this.mail;
	}
}