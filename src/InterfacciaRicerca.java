import java.util.Map;
import java.util.Map.Entry;

/**
 * Classe che implementa la boundary Visualizza Mansioni, 
 * che consente di visualizzare prima la lista delle mansioni 
 * e poi di scegliere il corso da organizzare e, in caso di errore,
 * lo notifica.
 */
public class InterfacciaRicerca implements GuiInterface {
	
	/**
	 * Metodo che implementa la visualizzazione di tutte le mansioni
	 * 
	 * @param mansioni Mappa che contiene il codice ed il nome del corso
	 */
	public void printListaMansioni(Map<Integer, String> mansioni) {
    System.out.println("L'HR ha selezionato: Organizza Corso\nBenvenuto nel sistema di organizzazione coso Leonardo\n");
	System.out.println("*******************-Interfaccia di Ricerca-*******************");
		
		// Visualizziamo la lista delle mansioni formattate, ottenibile in 3 modi diversi
		//Un ulteriore metodo sarebbe quello di conservare in 2 variabili il Set<Integer> delle chiavi e 
		//la Collection<String> dei valori e poi fare un for su entrambi (non � performante)
		System.out.println("Lista Mansioni:");
		System.out.println("|CODICE\t|NOME");

		//---------------------Vecchio modo---------------------//
		
		//---------------------1---------------------//
		
		// Get keys and values from mansioni Map
		//Usiamo mansioni.entrySet per ottenere il contenuto della mappa
		//for(Type varName : varList) itera la variabile varName di tipo Type all'interno della lista varList
		//N.B. tipoLista == Type
		for (Entry<Integer, String> entry : mansioni.entrySet()) {
			int k = entry.getKey();
			String v = entry.getValue();
			System.out.print("|" + k + "\t");
			System.out.println("|" + v);
				}
			}
		
		//---------------------2---------------------//
		
		//Mediante l'utilizzo di iteratori
		/*Iterator<Integer> codeIterator = mansioni.keySet().iterator();
		Iterator<String> nameIterator = mansioni.values().iterator();
		while(codeIterator.hasNext()) {
			System.out.print("|" + codeIterator.next() + "\t");
			System.out.println("|" + nameIterator.next());
		}*/
				
		//---------------------3---------------------//
		
		//Lambda Expression (equivalente a 1, lo dice anche nella descrizione del forEach)
		/*mansioni.forEach((key, value) -> {
			// N.B. Per stampare a video una riga e tornare a capo usare println(). print()
			// stampa sulla stessa riga
			System.out.print("|" + key + "\t");
			System.out.println("|" + value);
		});*/
	
/**
 * Metodo che notifica la possibilit� di inserire il codice del corso che si vuole organizzare
 */
 public  void SelezionaMans() {
	 
	 System.out.print("Inserisci codice corso: ");

 	}
 
 /**
  * Metodo che notifica che il codice inserito non � presente nella mappa delle mansioni
  */
 public void printErr() {
		 System.out.println("Codice non valido");
	 }
	 
 

}
	 
	 
	 