
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.Random;
/**
 * Classe che raccoglie metodi comuni e utili a piu classi 
 * 
 *
 */
 
public class Utility {
    /**
     * genera un numero intero random contenuto in (min,max) estremi inclusi
     *  
     * @param min 			estremo inferiore dell'insieme dei numeri random generati
     * @param max 			estremo superiore dell'insieme dei numeri random generati
     * @return 				int numero randomico contenuto in (min,max) estremi inclusi
     * @see 				java.util.Random
     */
    public static int getRandomNumberInRange(int min, int max) {
        Random r = new Random();
        return r.ints(min, (max + 1)).limit(1).findFirst().getAsInt();

 

    }
    /**
     * valida il formato di una stringa, restituisce false se la stringa non � valida, vero viceversa.
     * 
     * 
     * @param format formattazione desiderata
     * @param value	 stringa da validare
     * @param locale fuso orario
     * @return bool false se la stringa non � valida vero al contrario
     */
    public static boolean isValidFormat(String format, String value, Locale locale) {
        LocalDate ldt = null;
        DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, locale);

 

        try {
            ldt = LocalDate.parse(value, fomatter);
            String result = ldt.format(fomatter);
            return result.equals(value);
        } catch (DateTimeParseException e) {
            try { 
                LocalDate ld = LocalDate.parse(value, fomatter);
                String result = ld.format(fomatter);
                e.printStackTrace();
                return result.equals(value);
            } catch (DateTimeParseException e1) {
            	System.out.println("Formato data non valido");
          }
        }
        return false;
    }
}
 