import java.util.Calendar;
import java.util.List;
/**
 * rappresenta un Corso 
 * 
 */
public class Corso {
    private List<Dipendente> dipendenti;
    private Docente docente;
    private String aula;
    private Calendar data = Calendar.getInstance();
 /**
  * Metodo che inizializza il corso
  *    
  * @param dipendenti    List
  * @param docente	     Docente	
  * @param aula		     String
  * @param data		     Calendar
  */
    public void setCorso(List<Dipendente> dipendenti, Docente docente, String aula, Calendar data) {
        this.dipendenti = dipendenti;
        this.docente = docente;
        this.aula = aula;
        this.data = data;
    }
    /**
     *Metodo che permette di accedere ai dipendenti che devono seguire quel corso
     * 
     * @return List dipendenti
     */
    
    public List<Dipendente> getDipendenti() {
        return this.dipendenti;
    }
    /**
     *Metodo che permette di accedere al docente che deve tenere il corso
     * 
     * @return Docente docente	
     */
    public Docente getDocente() {
        return this.docente;
    }
    /**
     *Metodo che permette di accedere all'aula stabilita per il corso
     * 
     * @return String aula
     */
    public String getAula() {
        return this.aula;
    }
    /**
     *Metodo che permette di accedere alla data stabilita per il corso
     * 
     * @return Calendar data
     */
    public Calendar getData() {
        return this.data;
    }   
}