
import java.text.SimpleDateFormat;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 * Effettua convocazione tramite mail.
 * 
 *
 */
public class Convoca {

	/**
	 * Invia mail a destinatari multipli indicati nell'array recapiti;
	 * nella mail l'oggetto � "convocazione per il corso" 'nomeCorso' � passato come stringa in ingresso.
	 * nel corpo si scrive "Si comunica che il giorno 'data' il Prof. 'docente ' terr� il corso di ' nomeCorso ' 
	 * presso l'aula 'aula'.
	 * 
	 * @param recapiti  	String[] 
	 * @param nomeCorso		String 	 
	 * @param data			Calendar 
	 * @param aula			String	 
	 * @param docente		String	 
	 */
   public void inviaMail(String[] recapiti, String nomeCorso, Calendar data, String aula, String docente){    
	   //Dichiaro un array di InternetAddress
       InternetAddress[] recipientAddress = new InternetAddress[recapiti.length];
       //Riempio l'array
       int counter = 0;
       for (String recipient : recapiti) {
           try {
            recipientAddress[counter] = new InternetAddress(recipient.trim());
        } catch (AddressException e) {
            e.printStackTrace();
        }
           counter++;
       }
       
      //Mittente
      String from = "gruppo7formazioneleonardo@gmail.com";

      // Set dell'host 
      String host = "smtp.gmail.com";

      // Propriet� del sistema
      Properties properties = System.getProperties();

      //Setup mail server 
      properties.put("mail.smtp.host", host);
      properties.put("mail.smtp.port", "465");
      properties.put("mail.smtp.ssl.enable", "true");
      properties.put("mail.smtp.auth", "true");

      //Sessione : propriet�, username, password  
      Session session = Session.getInstance(properties, new Authenticator() {

          protected PasswordAuthentication getPasswordAuthentication() {
              
              return new PasswordAuthentication("gruppo7formazioneleonardo@gmail.com", "wpaxeclykdunprew");
          }
      });
      
      /*
       * alternativa alla classe interna anonima
       * passare al metodo getInstance un new Autenticatore(), oggetto della classe interna
       * realizzata esternamente al metodo inviaMail
       * 
       * class Autenticatore extends Authenticator{
       * protected Autenticatore () {}
       * protected PasswordAuthentication getPasswordAuthentication() {
       *  return new PasswordAuthentication("gruppo7formazioneleonardo@gmail.com", "wpaxeclykdunprew");
       *  }
       * }
       */
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        InterfacciaVisualizzaPlanning intViPlan = new InterfacciaVisualizzaPlanning();
      try {
    	 //Creazione di un oggetto di default MimeMessage
         MimeMessage message = new MimeMessage(session);
         
         //Set del mittente
         message.setFrom(new InternetAddress(from));
         
         //Set dei destinatari
         message.setRecipients(Message.RecipientType.TO, recipientAddress);

         //Campo oggetto
         message.setSubject("Convocazione per il corso " + nomeCorso);

         //Corpo della mail
         message.setText("Si comunica che il giorno " + new SimpleDateFormat("dd MMM yyyy").format(data.getTime()) + " il Prof. " + docente + " terr� il corso di " + nomeCorso + " presso l'aula " + aula + "." + "\nDistinti Saluti\nResponsabile HR -  Gruppo 7");

         //Invia il messaggio
         Transport.send(message);
         
         intViPlan.printMessaggio(nomeCorso, aula, sdf,data);
         
         
         
      } catch (MessagingException mex) {
    	 intViPlan.printErrSend(nomeCorso, aula, sdf,data);
         mex.printStackTrace();
      }
   }
}