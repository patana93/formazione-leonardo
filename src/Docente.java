import java.lang.String;
import com.fasterxml.jackson.annotation.JsonCreator;
/**
 * Classe che rappresenta un docente. 
 * E' specializzazione della superclasse Persona. 
 */
public class Docente extends Persona  {
	private String insegnamento;
	private String mail;
	private String numero;
	/**
	 * inizializza l'istanza Docente
	 * 
	 * @param nome String
	 * @param cognome String
	 * @param insegnamento String
	 * @param mail String
	 * @param numero String
	 */
	@JsonCreator
	public void initDoc(String nome, String cognome, String insegnamento, String mail, String numero) {
		super.PersonaInit(nome,cognome, mail);
		this.insegnamento = insegnamento;
		this.numero = numero;
	}
	
	/**
     * Metodo che accede all'insegnamento
     * 
     * @return String insegnamento
     */
	public String getInsegnamento() {
		return this.insegnamento;
	}
	/**
     * 
     * @return String mail, associato al Docente
     */
	public String getMail() {
		return this.mail;
	}
	/**
     * Metodo che accede al numero di telefono del Docente
     * 
     * @return String numero
     */
	public String getNumero() {
		return this.numero;
	}
	/**
	 * @return String nome associato al Docente
	 */
	public String getNome() {
		return this.nome;
	}
	/**
	 * @return String cognome associato al Docente
	 */
	
	public String getCognome() {
		return this.cognome;
	}
	
	
}

