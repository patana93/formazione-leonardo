
import java.util.List;


/**
 * Classe che implementa la boundary Interfaccia di Riepilogo, la quale
 *  consente di visualizzare prima le disponibilitÓ dell'aula, successivamente
 * la lista dei docenti disponibili per tale mansione ed il numero di corsi da organizzare.
 * In seguito permette di visualizzare i campi che l'utente deve compilare per portare
 * a termine l'organizzazione del corso.
 */
public class InterfacciaRiepilogo implements GuiInterface {

	/**
	 * Metodo che consente di visualizzare il numero di dipendenti con tale certificato
	 * scaduto o in scadenza ed il numero di corsi da organizzare per tali dipendenti
	 * 
	 * @param dipendentiFiltrati List
	 * @param nCorsi double
	 */
	public void printFiltraggio(List<Dipendente> dipendentiFiltrati, double nCorsi) {
		
		System.out.println("\n*******************-Interfaccia di riepilogo-*******************");
		System.out.print("\nDipendenti per il corso: ");
		System.out.println(dipendentiFiltrati.size());
		System.out.print("\nNumero corsi da organizzare: ");
		System.out.println((int) nCorsi);
		
	}
	
	/**
	 * Metodo che mostra la possibilitÓ di inserire un mese per poi visulizzare le disponibilitÓ delle aule.
	 */
	public void inserisciMese() {
        
        System.out.print("\nInserisci mese da 1 a 12 (0 per uscire): ");
     
       }
	
	/**
	 * Metodo che consente di visualizzare che il mese inserito non Ŕ valido
	 */
   public void printMesenNonValido() {
    System.out.print("Mese inserito non valido");
    }
   
   /**
    * Metodo che consente di visualizzare le disponibilitÓ delle aule per il mese scelto
    * @param aul 	Aula
    * @param month	int
    */
   public void printCalendar(Aula aul, int month) {
	  
	        System.out.println("Disponibilita' Aula " + aul.getId() + " :");
	        System.out.println(aul.printCalendar(month));
	     
	    }
   
   /**
    * Metodo che notifica l'inizio della compilazione del Form
    */
   public void printCompForm() {
	   
	   System.out.println("\n*******************-Compila Form-*******************");
   }
   
   /**
    * Metodo che notifica quale corso si sta organizzando
    * @param i 	 int
    * @param nCorsi int
    */
   public void printCorsoAttuale(int i, double nCorsi) {
	   System.out.println("Corso n: " + (i+1) + "/" + (int) nCorsi);
   }
   
   /**
    * Metodo che consente di visualizzare i dati dei dipendenti che devono seguire il corso scelto
    * @param startIndex int
    * @param endIndex int
    * @param dipendentiFiltrati List
    */
   public void printDipendentiFiltrati(int startIndex, int endIndex, List<Dipendente> dipendentiFiltrati) {
	   
	   System.out.println("\n|MATRICOLA\t\t|NOME\t\t|COGNOME\t\t|MANSIONE\t\t|CERTIFICATO\t\t|STATO\t\t|SCADENZA\t\t|RECAPITO");
        for(Dipendente dipFil: dipendentiFiltrati.subList(startIndex, endIndex)) {
        	System.out.print(dipFil.getMatricola() + "\t\t\t");
            System.out.print(dipFil.getNome() + "\t\t");
            System.out.print(dipFil.getCognome() + "\t\t");
            System.out.print(dipFil.getMansione() + "\t\t");
            System.out.print(dipFil.getCertificato() + "\t\t");
            System.out.print(dipFil.getStato() + "\t\t");
            System.out.print(dipFil.getScadenza().getTime() + "\t\t");
            System.out.println(dipFil.getMail());    
        }
   }
   
   /**
    * Metodo che consente di visualizzare i dati dei docenti che possono tenere il corso scelto
    * @param docentiFiltrati List
    */
   public void printDocentiFiltrati(List<Docente> docentiFiltrati) {
	   System.out.println("\nDocenti per il corso: ");
		System.out.println("Nome|\t\tCognome|\t\tInsegnamento|\t\tMail|\t\t\t\tNumero");
		for (int i = 0; i<docentiFiltrati.size(); i++) {
			System.out.print((i+1) + ")");
			System.out.print(docentiFiltrati.get(i).getNome() + "\t\t");
			System.out.print(docentiFiltrati.get(i).getCognome() + "\t\t");
			System.out.print(docentiFiltrati.get(i).getInsegnamento() + "\t\t");
			System.out.print(docentiFiltrati.get(i).getMail() + "\t\t");
			System.out.println(docentiFiltrati.get(i).getNumero());
		}  
   }
   
   /**
    * Metodo che notifica la possibilitÓ di selezionare un docente
    * @param docentiFiltrati List
    */
   public void printSelDocente(List<Docente> docentiFiltrati) {
	   System.out.print("Seleziona docente (da 1 a " + docentiFiltrati.size() + "): ");
   }
   
   /**
    * Metodo che notifica l'inserimento di un formato non valido
    */
   public void printErr() {
		 System.out.println("Formato non valido");
	 }
   
   /**
    * Metodo che printa l'intestazione per la lista dei docenti tale insegnamento
    */
   public void  printIntestazioneDocente () {
	      
       System.out.println("\nLista Docenti:");
   }
   
   /**
    * Metodo che notifica la possibilitÓ di selezionare un'aula
    * @param aule List
    */
   public void  printSelAula (List<Aula> aule) {
	   
       System.out.print("Seleziona aula (da 1 a " + aule.size() + "):");
   }
   
   /**
    * Metodo che printa l'intestazione per la lista delle aule
    */
   public void printIntAule() {
	   System.out.println("\nLista aule:");
   }
   
   /**
    * Metodo che consente di visualizzare l'elenco delle aule
    * @param aule List
    */
   public void printDispAule(List<Aula> aule) {
       for(int j = 0; j < aule.size(); j++) {
            System.out.println((j + 1) + ")" + aule.get(j).getId());
        }
       }
   
   /**
    * Metodo che notifica la possibilitÓ di scegliere una data per il corso che si sta organizzando
    */
   public void  printSelData () {
       
       System.out.print("Inserisci la data del corso (DD-MM-YYYY): ");
   }
   
   /**
    * Metodo che notifica che la data selezionata Ŕ antecedente a quella attuale
    */
   public void  printDataAnte() {
       
	     System.out.println("Data antecedente ad oggi");
	       }
   
   /**
    * Metodo che notifica che la data selezionata Ŕ oltre un anno dal mese attuale
    */
   public void  printDataFuoriRange() {
	      
	    System.out.println("Data successiva al range consentito (1 anno dal mese attuale)");
	       }

   /**
    * Metodo che notifica la non disponibilitÓ dell'aula per il giorno selezionato
    */
   public void  printDataNonDisp() {
    
       System.out.println("Aula non disponibile in questa data");
        }
   
   /**
    * Metodo che notifica che il valore inserito non Ŕ valido
    */
   public void printValNonValido () {
	   
	   System.out.println("Valore non valido");
   }

   
  
 }

	

