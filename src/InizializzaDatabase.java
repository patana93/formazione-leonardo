import java.util.*;
import java.lang.String;
/*
 Questa classe � stata creata per generare il file che conserva le entit�

*/
public class InizializzaDatabase {
	/*******************-Dipendente-*******************/  
    public List<Dipendente> inizializzaDbDip (){
        
    	
        String[] firstNameArray =  new String[] { "Adam", "Alex", "Aaron", "Ben", "Carl", "Dan", "David", "Edward", "Fred", "Frank", "George", "Hal", "Hank", "Ike", "John", "Jack", "Joe", "Larry", "Monte", "Matthew", "Mark", "Nathan", "Otto", "Paul", "Peter", "Roger", "Roger", "Steve", "Thomas", "Tim", "Ty", "Victor", "Walter"};        
        String[] lastNameArray = new String[] { "Anderson", "Ashwoon", "Aikin", "Bateman", "Bongard", "Bowers", "Boyd", "Cannon", "Cast", "Deitz", "Dewalt", "Ebner", "Frick", "Hancock", "Haworth", "Hesch", "Hoffman", "Kassing", "Knutson", "Lawless", "Lawicki", "Mccord", "McCormack", "Miller", "Myers", "Nugent", "Ortiz", "Orwig", "Ory", "Paiser", "Pak", "Pettigrew", "Quinn", "Quizoz", "Ramachandran", "Resnick", "Sagar", "Schickowski", "Schiebel", "Sellon", "Severson", "Shaffer", "Solberg", "Soloman", "Sonderling", "Soukup", "Soulis", "Stahl", "Sweeney", "Tandy", "Trebil", "Trusela", "Trussel", "Turco", "Uddin", "Uflan", "Ulrich", "Upson", "Vader", "Vail", "Valente", "Van Zandt", "Vanderpoel", "Ventotla", "Vogal", "Wagle", "Wagner", "Wakefield", "Weinstein", "Weiss", "Woo", "Yang", "Yates", "Yocum", "Zeaser", "Zeller", "Ziegler", "Bauer", "Baxster", "Casal", "Cataldi", "Caswell", "Celedon", "Chambers", "Chapman", "Christensen", "Darnell", "Davidson", "Davis", "DeLorenzo", "Dinkins", "Doran", "Dugelman", "Dugan", "Duffman", "Eastman", "Ferro", "Ferry", "Fletcher", "Fietzer", "Hylan", "Hydinger", "Illingsworth", "Ingram", "Irwin", "Jagtap", "Jenson", "Johnson", "Johnsen", "Jones", "Jurgenson", "Kalleg", "Kaskel", "Keller", "Leisinger", "LePage", "Lewis", "Linde", "Lulloff", "Maki", "Martin", "McGinnis", "Mills", "Moody", "Moore", "Napier", "Nelson", "Norquist", "Nuttle", "Olson", "Ostrander", "Reamer", "Reardon", "Reyes", "Rice", "Ripka", "Roberts", "Rogers", "Root", "Sandstrom", "Sawyer", "Schlicht", "Schmitt", "Schwager", "Schutz", "Schuster", "Tapia", "Thompson", "Tiernan", "Tisler" };
    
        Map<Integer,String> mansioni = inizializzaDbMans();
        
        List<String> mansioniCertificato = new ArrayList<String>(mansioni.values());
        
        List<Dipendente> listaDipDb = new ArrayList<Dipendente>();
        
        
        for (int i = 0; i < 1500; i++) {
            String nome = firstNameArray[Utility.getRandomNumberInRange(0, firstNameArray.length - 1)];
            String cognome = lastNameArray[Utility.getRandomNumberInRange(0, lastNameArray.length - 1)];
            String mansione = mansioniCertificato.get(Utility.getRandomNumberInRange(0, mansioni.size()-1));
                
            int year = Utility.getRandomNumberInRange(2020, 2022);
            int month = Utility.getRandomNumberInRange(0, 11);
            int day = Utility.getRandomNumberInRange(1, 30);
            
            Dipendente dipAttuale = new Dipendente();
            dipAttuale.initDip(i, nome, cognome, mansione, mansione, year, month, day, nome + "." + cognome + i + "@gmail.com");
            
            listaDipDb.add(dipAttuale);
        }
  
    return listaDipDb;
    
    
    
    }
	/*******************-Docente-*******************/
    public List<Docente> inizializzaDbDoc() {
        
        String[] firstNameArray =  new String[] { "Adam", "Alex", "Aaron", "Ben", "Carl", "Dan", "David", "Edward", "Fred", "Frank", "George", "Hal", "Hank", "Ike", "John", "Jack", "Joe", "Larry", "Monte", "Matthew", "Mark", "Nathan", "Otto", "Paul", "Peter", "Roger", "Roger", "Steve", "Thomas", "Tim", "Ty", "Victor", "Walter"};   
        
        String[] lastNameArray = new String[] { "Anderson", "Ashwoon", "Aikin", "Bateman", "Bongard", "Bowers", "Boyd", "Cannon", "Cast", "Deitz", "Dewalt", "Ebner", "Frick", "Hancock", "Haworth", "Hesch", "Hoffman", "Kassing", "Knutson", "Lawless", "Lawicki", "Mccord", "McCormack", "Miller", "Myers", "Nugent", "Ortiz", "Orwig", "Ory", "Paiser", "Pak", "Pettigrew", "Quinn", "Quizoz", "Ramachandran", "Resnick", "Sagar", "Schickowski", "Schiebel", "Sellon", "Severson", "Shaffer", "Solberg", "Soloman", "Sonderling", "Soukup", "Soulis", "Stahl", "Sweeney", "Tandy", "Trebil", "Trusela", "Trussel", "Turco", "Uddin", "Uflan", "Ulrich", "Upson", "Vader", "Vail", "Valente", "Van Zandt", "Vanderpoel", "Ventotla", "Vogal", "Wagle", "Wagner", "Wakefield", "Weinstein", "Weiss", "Woo", "Yang", "Yates", "Yocum", "Zeaser", "Zeller", "Ziegler", "Bauer", "Baxster", "Casal", "Cataldi", "Caswell", "Celedon", "Chambers", "Chapman", "Christensen", "Darnell", "Davidson", "Davis", "DeLorenzo", "Dinkins", "Doran", "Dugelman", "Dugan", "Duffman", "Eastman", "Ferro", "Ferry", "Fletcher", "Fietzer", "Hylan", "Hydinger", "Illingsworth", "Ingram", "Irwin", "Jagtap", "Jenson", "Johnson", "Johnsen", "Jones", "Jurgenson", "Kalleg", "Kaskel", "Keller", "Leisinger", "LePage", "Lewis", "Linde", "Lulloff", "Maki", "Martin", "McGinnis", "Mills", "Moody", "Moore", "Napier", "Nelson", "Norquist", "Nuttle", "Olson", "Ostrander", "Reamer", "Reardon", "Reyes", "Rice", "Ripka", "Roberts", "Rogers", "Root", "Sandstrom", "Sawyer", "Schlicht", "Schmitt", "Schwager", "Schutz", "Schuster", "Tapia", "Thompson", "Tiernan", "Tisler" };
        
        Map<Integer,String> mansioni = inizializzaDbMans();
        
        List<String> insegnamentoCertificato = new ArrayList<String>(mansioni.values());
        
        List<Docente> listaDocDb = new ArrayList<Docente>();
        
        
        
        for (int i = 0; i < 20; i++) {
            String nome = firstNameArray[Utility.getRandomNumberInRange(0, firstNameArray.length - 1)];
            String cognome = lastNameArray[Utility.getRandomNumberInRange(0, lastNameArray.length - 1)];
            String insegnamento = insegnamentoCertificato.get(Utility.getRandomNumberInRange(0,insegnamentoCertificato.size()-1));
            
            
            Docente docAttuale = new Docente();
            docAttuale.initDoc(nome, cognome, insegnamento, nome + "." + cognome + i + i + "@gmail.com", "081876593" + i);
            
            listaDocDb.add(docAttuale);
        }
        
        return listaDocDb;
    }
	/*******************-Mansioni-*******************/
    private Map<Integer, String> mansioni = new HashMap<Integer, String>();

    private Mansione mansione1 = new Mansione ();
    private Mansione mansione2 = new Mansione ();
    private Mansione mansione3 = new Mansione ();
    private Mansione mansione4 = new Mansione ();
    private Mansione mansione5 = new Mansione ();
    private Mansione mansione6 = new Mansione ();
    private Mansione mansione7 = new Mansione ();
    private Mansione mansione8 = new Mansione ();
    private Mansione mansione9 = new Mansione ();
    private Mansione mansione10 = new Mansione ();
    
    public Map<Integer, String> inizializzaDbMans() {
        
        mansione1.initMansione("Meccanico impiantista", 11);
        mansione2.initMansione("Security HR", 22);
        mansione3.initMansione("Security LR", 33);
        mansione4.initMansione("Product Assurance Technician", 44);
        mansione5.initMansione("Aereostrutture", 55);
        mansione6.initMansione("Radar", 66);
        mansione7.initMansione("Profilo Alare", 77);
        mansione8.initMansione("Elettrotecnica", 88);
        mansione9.initMansione("Antenne", 99);
        mansione10.initMansione("Fusoliera",111);
        
        //Creo la mappa associando al codice (K, chiave) della singola mansione il suo nome (V, valore)
        mansioni.put(mansione1.getCodice(), mansione1.getNome());
        mansioni.put(mansione2.getCodice(), mansione2.getNome());
        mansioni.put(mansione3.getCodice(), mansione3.getNome());
        mansioni.put(mansione4.getCodice(), mansione4.getNome());
        mansioni.put(mansione5.getCodice(), mansione5.getNome());
        mansioni.put(mansione6.getCodice(), mansione6.getNome());
        mansioni.put(mansione7.getCodice(), mansione7.getNome());
        mansioni.put(mansione8.getCodice(), mansione8.getNome());
        mansioni.put(mansione9.getCodice(), mansione9.getNome());
        mansioni.put(mansione10.getCodice(), mansione10.getNome());
        
        return mansioni;
    }
    
	/*******************-Aula-*******************/
    List<Aula> aule = new ArrayList<Aula>();
    Aula aula1 = new Aula();
    Aula aula2 = new Aula();
    Aula aula3 = new Aula();

    public List<Aula> inizializzaDbAula() {
        
        List<GregorianCalendar> disponibilitaAulaList = new ArrayList<GregorianCalendar>();

        for (int i = 0; i < 350; i++) {
            int year = Utility.getRandomNumberInRange(2020, 2022);
            int month = Utility.getRandomNumberInRange(0, 11);
            int day = Utility.getRandomNumberInRange(1, 30);
            GregorianCalendar calendar = new GregorianCalendar(year, month, day);
            disponibilitaAulaList.add(calendar);
        }
        
        aula1.initAula("TA1", disponibilitaAulaList);
        
        List<GregorianCalendar> disponibilitaAulaList2 = new ArrayList<GregorianCalendar>();
        
        for (int i = 0; i < 350; i++) {
            int year = Utility.getRandomNumberInRange(2020, 2022);
            int month = Utility.getRandomNumberInRange(0, 11);
            int day = Utility.getRandomNumberInRange(1, 30);
            GregorianCalendar calendar = new GregorianCalendar(year, month, day);
            disponibilitaAulaList2.add(calendar);
        }
        
        aula2.initAula("IA2", disponibilitaAulaList2);
        
        List<GregorianCalendar> disponibilitaAulaList3 = new ArrayList<GregorianCalendar>();
        
        for (int i = 0; i < 350; i++) {
            int year = Utility.getRandomNumberInRange(2020, 2022);
            int month = Utility.getRandomNumberInRange(0, 11);
            int day = Utility.getRandomNumberInRange(1, 30);
            GregorianCalendar calendar = new GregorianCalendar(year, month, day);
            disponibilitaAulaList3.add(calendar);
        }
        
        aula3.initAula("IIA3", disponibilitaAulaList3);        
        
        aule.add(aula1);
        aule.add(aula2);
        aule.add(aula3);
        
        return (aule);
    }
}