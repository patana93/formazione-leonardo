import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.lang.String;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Classe che implementa il caso d'uso principale Organizza Corso. 
 * Viene prelevato il codice del corso che si vuole organizzare e, se questo � valido, si passa al filtraggio dei docenti che possono
 * svolgere quel corso, al filtraggio dei dipendenti che hanno tale mansione con certificato scaduto o in scadenza.
 * Successivamente, in base al numero di dipendenti che devono rinnovare quel certificato e alla capienza delle aule,
 * vengono calcolati il numero di corsi da svolgere.
 * Viene poi data la possibilit� di compilare i form per portare a termine l'organizzazione dei corsi.
 * Infine viene implementato il planning dei corsi appena organizzati e vengono inviate le mail  al docente e ai dipendenti  
 * con tutte le informazioni relative al nuovo corso.
 */
public class OrganizzaCorso {

	public static void main(String[] args) {
		// Per la creazione dei files
		// InizializzaDatabase myDatabase = new InizializzaDatabase();
		// List<Dipendente> dipendenti = myDatabase.inizializzaDbDip();
		// List<Docente> docenti = myDatabase.inizializzaDbDoc();
		// Map<Integer, String> mansioni = myDatabase.inizializzaDbMans();
		// List<Aula> aule = myDatabase.inizializzaDbAula();

		// Dichiarazione delle entit�
		List<Corso> newCorsi = new ArrayList<Corso>();
		List<Dipendente> dipendenti = new ArrayList<Dipendente>();
		List<Docente> docenti = new ArrayList<Docente>();
		Map<Integer, String> mansioni = new HashMap<Integer, String>();
		List<Aula> aule = new ArrayList<Aula>();
		ObjectMapper mapper = new ObjectMapper();
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM yyyy");
		mapper.setDateFormat(sdf2);

		// Lettura dei files

		try {
			dipendenti = Arrays.asList(mapper.readValue(new File(".\\Database\\Dipendenti.json"), Dipendente[].class));
			docenti = Arrays.asList(mapper.readValue(new File(".\\Database\\Docenti.json"), Docente[].class));
			mansioni = mapper.readValue(new File(".\\Database\\Mansioni.json"),
					new TypeReference<Map<Integer, String>>() {
					});
			aule = Arrays.asList(mapper.readValue(new File(".\\Database\\Aule.json"), Aula[].class));
		} catch (IOException e) {
			// e.printStackTrace();
			System.err.println("Errore nella lettura dei files :/ \nRiavvia l'applicazione");
			System.exit(1);
		}

		/******************* -Scrittura files- *******************/
		/*
		 * // ---------------------File dipendente---------------------// try {
		 * mapper.writeValue(new File(".\\Database\\Dipendenti.json"), dipendenti); }
		 * catch (IOException e) { e.printStackTrace(); } // ---------------------File
		 * docente---------------------//
		 * 
		 * try { mapper.writeValue(new File(".\\Database\\Docenti.json"), docenti); }
		 * catch (IOException e) { e.printStackTrace(); }
		 * 
		 * // ---------------------File mansione---------------------//
		 * 
		 * try { mapper.writeValue(new File(".\\Database\\Mansioni.json"), mansioni); }
		 * catch (IOException e) { e.printStackTrace(); }
		 * 
		 * // ---------------------File aule---------------------//
		 * 
		 * try {
		 * 
		 * mapper.writeValue(new File(".\\Database\\Aule.json"), aule); } catch
		 * (IOException e) { e.printStackTrace(); }
		 */
		/******************* -Visualizza Lista Mansioni- *******************/

		InterfacciaRicerca intRic = new InterfacciaRicerca();

		intRic.printListaMansioni(mansioni);

		/******************* -Ricerca Mansione- *******************/

		boolean trovato = false;
		Scanner scanner = new Scanner(System.in);
		String nomeCorso = "";

		do {
			int codiceCorso = 0;

			try {
				intRic.SelezionaMans();
				codiceCorso = Integer.parseInt(scanner.next());
				trovato = Mansione.ControlloCodCorso(codiceCorso, mansioni);
			} catch (java.lang.NumberFormatException e) {
				scanner.reset();
			}
			if (!trovato) {
				intRic.printErr();
			} else {
				nomeCorso = mansioni.get(codiceCorso);
			}
		} while (!trovato);

		/******************* -Filtraggio- *******************/
		InterfacciaRiepilogo intRiep = new InterfacciaRiepilogo();
		List<Docente> docentiFiltrati = new ArrayList<Docente>();
		List<Dipendente> dipendentiFiltrati = new ArrayList<Dipendente>();

		// Filtriamo la lista docenti in base alla lezione
		for (Docente doc : docenti) {
			if (doc.getInsegnamento().contains(nomeCorso)) {
				docentiFiltrati.add(doc);
			}
		}

		// Filtriamo la lista dipendenti in base alla mansione

		Calendar dataScadenza = Calendar.getInstance();
		for (Dipendente dip : dipendenti) {
			if (dip.getMansione().contains(nomeCorso)) {
				dataScadenza = dip.getScadenza();
				dip.setStato(dataScadenza);
				if (!dip.getStato().equals(Stato.NON_SCADUTO)) {
					dipendentiFiltrati.add(dip);
				}
			}
		}

		

		// Calcolo numero corsi
		double nCorsi = (double) dipendentiFiltrati.size() / 20;
		nCorsi = Math.ceil(nCorsi);
		intRiep.printFiltraggio(dipendentiFiltrati, nCorsi);
		intRiep.printDocentiFiltrati(docentiFiltrati);
		/******************* -Interfaccia di riepilogo- *******************/

		int month = 1;
		// Visualizza la disponibilit� delle aule in base al mese richiesto
		do {
			try {
				// Selezione del mese da 1 a 12 per visualizzare calendario.
				// Se il mese richiesto � antecedente a quello attuale, si visualizza quello
				// dell'anno successivo.
				do {
					intRiep.inserisciMese();
					month = Integer.parseInt(scanner.next());

					int nMese = Calendar.getInstance().get(Calendar.MONTH) + 1;
					if (month < 0 || month > 12) {
						intRiep.printMesenNonValido();
						month = -1;
					} else if (month < nMese && month != 0) {
						if (month == 1) {
							month += 24;
						} else {
							month += 12;
						}
					}
				} while (month == -1);

				if (month != 0) {
					for (Aula aul : aule) {
						
						intRiep.printCalendar(aul, month);
						

					}
				}

			} catch (java.lang.NumberFormatException e) {
				intRiep.printErr();
				scanner.reset();
			}
		} while (month != 0);


		/******************* -Compila Form- *******************/
		intRiep.printCompForm();

		int capienza = new Aula().posti();
		int startIndex = 0;
		int endIndex = capienza;
		int docSel = 0;
		int aulSel = 0;
		GregorianCalendar corsoCal = null;

		// Istanzia tanti corsi quanto sono necessari (in funzione di nCorsi)
		for (int i = 0; i < nCorsi; i++) {
			intRiep.printCorsoAttuale(i, nCorsi);
			// scorri la lista di dipendenti filtrati e prendine tanti quat'� la capienza
			// delle aule
			if (dipendentiFiltrati.size() < capienza) {
				endIndex = dipendentiFiltrati.size();
			}

			// Mostra i dipendenti filtrati per il corso considerato
			intRiep.printDipendentiFiltrati(startIndex, endIndex, dipendentiFiltrati);
			intRiep.printIntestazioneDocente();
			intRiep.printDocentiFiltrati(docentiFiltrati);

			// Seleziona docente dalla lista mostrata
			boolean selezionato = false;

			do {
				try {
					intRiep.printSelDocente(docentiFiltrati);
					docSel = Integer.parseInt(scanner.next());
					if (docSel > 0 && docSel <= docentiFiltrati.size()) {
						selezionato = true;
					} else {
						intRiep.printValNonValido();

					}
				} catch (java.lang.NumberFormatException e) {
					intRiep.printErr();
					scanner.reset();
				}

			} while (!selezionato);

			// Mostra Aule
			selezionato = false;

			intRiep.printIntAule();
			intRiep.printDispAule(aule);

			// Seleziona Aula
			do {
				try {
					intRiep.printSelAula(aule);
					aulSel = Integer.parseInt(scanner.next());
					if (aulSel > 0 && aulSel <= aule.size()) {
						selezionato = true;
					} else {
						intRiep.printValNonValido();
					}

				} catch (java.lang.NumberFormatException e) {
					intRiep.printErr();
					scanner.reset();
				}

			} while (!selezionato);

			// Inserisci data nel formato richiesto
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String dataString = "";
			selezionato = false;
			Date dataSel = new Date();
			corsoCal = new GregorianCalendar();
			do {
				try {
					intRiep.printSelData();
					dataString = scanner.next();
					dataSel = sdf.parse(dataString);
					corsoCal.setTime(dataSel);
					Calendar nextYear = Calendar.getInstance();
					nextYear.set(Calendar.getInstance().get(Calendar.YEAR) + 1,
							Calendar.getInstance().get(Calendar.MONTH), 0);

					if (Utility.isValidFormat("dd-MM-yyyy", dataString, Locale.getDefault())) {

						if ((dataSel.after(Calendar.getInstance().getTime()) && dataSel.before(nextYear.getTime()))
								&& !aule.get(aulSel - 1).getDisponibilita().contains(corsoCal)) {
							aule.get(aulSel - 1).addCalendar(corsoCal);
							selezionato = true;
						} else if (!dataSel.after(Calendar.getInstance().getTime())) {
							intRiep.printDataAnte();
						} else if (!dataSel.before(nextYear.getTime())) {
							intRiep.printDataFuoriRange();
						} else if (aule.get(aulSel - 1).getDisponibilita().contains(corsoCal)) {
							intRiep.printDataNonDisp();
						}
					}

				} catch (ParseException e) {
					intRiep.printErr();
					scanner.reset();
				}
			} while (!selezionato);
			// Inizializza il corso appena organizzato
			Corso newCorso = new Corso();
			newCorso.setCorso(dipendentiFiltrati.subList(startIndex, endIndex), docentiFiltrati.get(docSel - 1),
					aule.get(aulSel - 1).getId(), corsoCal);
			newCorsi.add(newCorso);

			startIndex += capienza;
			endIndex += capienza;

			if (endIndex >= dipendentiFiltrati.size()) {
				endIndex = dipendentiFiltrati.size();
			}
		}

		scanner.close();

		// Aggiorna dati disponibilit� aule su file
		try {
			mapper.writeValue(new File(".\\Database\\Aule.json"), aule);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Scrittura file aule fallita   :/");
		}
		// Aggiorna file corsi
		List<Corso> corsi = new ArrayList<Corso>();
		try {
			List<Corso> oldCorsi = Arrays.asList(mapper.readValue(new File(".\\Database\\Corsi.json"), Corso[].class));
			corsi = Stream.concat(oldCorsi.stream(), newCorsi.stream()).collect(Collectors.toList());
			mapper.writeValue(new File(".\\Database\\Corsi.json"), corsi);
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println("Scrittura file corsi fallita   :/");
		}

		/******************* -Visualizza Plannig- *******************/

		InterfacciaVisualizzaPlanning intPlan = new InterfacciaVisualizzaPlanning();
		intPlan.printVisPlan();
		for (Corso corso : newCorsi) {
			intPlan.printVisualizzaPlanning(corso);
		}

		/******************* -Convoca- *******************/

		intPlan.printConvoca();
		@SuppressWarnings("unused")
		Convoca convoca = new Convoca();
		//List<String> recapiti = new ArrayList<String>();
		for (int i = 0; i < nCorsi; i++) {

			// Se le mail corrispondenti ai dipendenti fossero valide faremmo
			/*
			 * for(Dipendente dip : corsi.get(i).getDipendenti()) {
			 * recapiti.add(dip.getRecapito()); }
			 * recapiti.add(corsi.get(i).getDocente().getMail()); String[] recap = new
			 * String[recapiti.size()]; recapiti.toArray(recap);
			 */

			// Dato che le mail utilizzate sono fittizie, inseriamo manualmente le nostre
			// mail per testare il programma
			
			
			String[] recap = new String[2];
			if (i == 0) {
				// recap[0] = "giuseppe.aceto@unina.it";
				recap[0] = "carlo.palumbox@gmail.com";
				recap[1] = "silviattr95@gmail.com";
			} else if (i == 1) {
				recap[0] = "alessiacordova2409@gmail.com";
				recap[1] = "illianojessica@hotmail.it";
			} else if (i == 2) {
				recap[0] = "s.tutore@studenti.unina.it";
				recap[1] = "illianojessica@hotmail.it";
			} else {
				recap[0] = "carlo.palumbox@gmail.com";
				recap[1] = "alessand.gagliardi@studenti.unina.it";
			}

			// convoca.inviaMail(recap, corsi.get(i).getDocente().getInsegnamento(),
			// corsi.get(i).getData(), corsi.get(i).getAula(),
			// corsi.get(i).getDocente().getCognome() + " " +
			// corsi.get(i).getDocente().getNome());
			intPlan.printConvEnd(i);
		}
		intPlan.printEnd();
		System.exit(0);
	}
}
