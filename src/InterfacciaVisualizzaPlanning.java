import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 *Classe che implementa la boundary Visualizza Planning che consente di visulizzare i corsi organizzati
 *e la notifica della convocazione del docente e dei dipendenti per svolgere il corso selezionato. 
 */
public class InterfacciaVisualizzaPlanning implements GuiInterface {
	
	/**
	 * Metodo che notifica l'inizio della visualizzazione del planning
	 */
	public void printVisPlan() {
		System.out.println("\n*******************-Visualizza Planning-*******************");
	}
	
	/**
	 * Metodo che mostra il planning dei corsi organizzati
	 * @param corso Corso
	 */
	 public void printVisualizzaPlanning(Corso corso) {
		   
			   System.out.println("\nDipendenti:");
		        System.out.println("|MATRICOLA\t\t|NOME\t\t|COGNOME\t\t|MANSIONE\t\t|CERTIFICATO\t\t|STATO\t\t|SCADENZA\t\t|RECAPITO");
		        for(Dipendente dip: corso.getDipendenti()) {
		        	System.out.print(dip.getMatricola()+ "\t\t\t");
		            System.out.print(dip.getNome() + "\t\t");
		            System.out.print(dip.getCognome() + "\t\t");
		            System.out.print(dip.getMansione() + "\t\t");
		            System.out.print(dip.getCertificato() + "\t\t");
		            System.out.print(dip.getStato() + "\t\t");
		            System.out.print(dip.getScadenza().getTime() + "\t\t");
		            System.out.println(dip.getMail());
		       
		        }
		        System.out.println("\nDocente:");
		        System.out.println("Nome|\t\tCognome|\t\tInsegnamento|\t\tMail|\t\t\t\tNumero");
		        System.out.println(corso.getDocente().getNome()+"\t\t"+corso.getDocente().getCognome()+ "\t\t"+ corso.getDocente().getInsegnamento()+"\t\t"+ corso.getDocente().getMail()+"\t\t"+ corso.getDocente().getNumero());
		        System.out.print("\nAula corso: ");
		        System.out.println(corso.getAula());
		        System.out.print("\nGiorno corso: ");
		        System.out.println(corso.getData().getTime());
			
	   }
	 
	 /**
	  * Metodo che consente di indicare l'inizio della fase di convocazione
	  */
	 public void printConvoca() {
		 System.out.println("\n*******************-Convoca-*******************");
		 System.out.println("\nConvocazione in corso...Attendere prego");
		}
	 
	 /**
	  * Metodo che consente di visualizzare che le mail sono state inviate con successo
	  * @param nomeCorso String
	  * @param aula String
	  * @param sdf SimpleDateFormat
	  * @param data	Calendar
	  * @see java.text.SimpleDateFormat
	  * @see java.util.Calendar
	  * @see Convoca
	  */
	 public void printMessaggio(String nomeCorso,String aula, SimpleDateFormat sdf, Calendar data  ) {
		 System.out.println("Il messaggio per il corso " + nomeCorso + " presso l'aula " + aula + " del " + sdf.format(data.getTime()) + " � stato inviato con successo... ;)");
	 }
	 
	 /**
	  * Metodo che notifica un'errore nell'invio delle mail
	  * @param nomeCorso String
	  * @param aula String
	  * @param sdf SimpleDateFormat
	  * @param data Calendar
	  * @see java.text.SimpleDateFormat
	  * @see java.util.Calendar
	  * @see Convoca
	  */
	 public void printErrSend(String nomeCorso, String aula, SimpleDateFormat sdf, Calendar data) {
		 System.out.println("Errore nell'invio della mail per il corso: " + nomeCorso + " presso l'aula " + aula + " del " + sdf.format(data.getTime()));
	 }
	 
	 /**
	  * Metodo che notifica che la convocazione del corso � terminata
	  * @param i int
	  */
	 public void printConvEnd(int i) {
		 System.out.println("Convocazione corso " + (i+1) + " terminata.\n");
	 }
	 
	 /**
	  * Metodo che notifica che le convocazioni sono andate a buon fine
	  */
	 public void printEnd() {
		 System.out.println("Organizzazione corso/i terminata.\nGrazie per aver utilizzato il nostro software.");
	 }

	public void printErr() {
		// TODO per implementazioni future
	}
	
	 
}
