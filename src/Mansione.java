import java.lang.String;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Classe che rappresenta una mansione dell'azienda
 * 
 */
public class Mansione {
	
	 private String nome;
	 private int codice;
	  
	/**
	 * Metodo per inizializzare una mansione
	 * 
	 * @param nome    String
	 * @param codice  int
	 */
	 @JsonCreator
	 public void initMansione(String nome, int codice) {
		 this.nome = nome;
		 this.codice = codice;
		 
	 }
	 /**
	  * Metodo per accedere all'attributo nome della mansione
	  * 
	  * @return   stringa associata all'attributo 'nome'
	  */
	 public String getNome() {
		 return this.nome;
	 }
	 /**
	  *  Metodo per accedere all'attributo nome della mansione
	  *  
	  * @return   intero associato all'attributo 'codice'
	  */
	 public int getCodice() {
		 return this.codice;
	 }
     /**
      * Metodo per controllare che il codice inserito sia valido
      * 
      * @param codCorso  int
      * @param mansioni  Map 
      * 
      * @return          booleano che indica se il codice del corso � stato 'trovato' 
      */
	 public static boolean ControlloCodCorso(int codCorso, Map<Integer, String> mansioni) {
	 boolean trovato = false;
	 trovato = mansioni.containsKey(codCorso);
	 return trovato ;
 }


}