/**
 * Superclasse che rappresenta un generico membro dell'azienda 
 * 
 */

public class Persona {
	protected String nome;
	protected String cognome;
	protected String mail;
	
	/**
	 * Metodo per inizializzare gli attributi che identificano una persona
	 * 
	 * @param nome String
	 * @param cognome String
	 * @param mail String
	 */
	protected void PersonaInit(String nome, String cognome, String mail) {
		this.nome = nome;
		this.cognome = cognome;
		this.mail = mail;
	}
	
	/**
	 * Metodo che permette di accedere all'attributo nome
	 * 
	 * @return     attributo 'nome', associato alla persona
	 */
	protected String getNome() {
		return this.nome;
	}
	
	/**
	 * Metodo che permette di accedere all'attributo cognome
	 * 
	 * @return    attributo 'cognome', associato alla persona 
	 */
	protected String getCognome() {
		return this.cognome;
	}
    /**
     * Metodo che permette di accedere all'attributo mail
     * 
     * @return  attributo 'mail', associato alla persona
     */
	protected String getMail() {
		return this.mail;
	}
}
